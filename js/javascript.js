﻿$(document).ready(function(){
	google.charts.load('current', {packages: ['corechart', 'line','bar']});
	$.getJSON( "js/sales_summary.json", function( data ) {

	  	$.each( data, function( key, val ) {
	    	$("#"+key).append(val.toLocaleString('pt'));
		}); 
	});
	$.getJSON( "js/errors.json", function( data ) {

	  	$.each( data, function( key, val ) {
	    	$("#"+key).append(val.toLocaleString('pt'));
		}); 
	});


	$.getJSON( "js/sales_geochart.json", function( data ) {
		i = 0;
	  	$.each( data, function( key, val ) {
	  		if(i==0){
	    		$("#sales_geochart thead").append("<th>"+val[0]+"</th><th>"+val[1]+"</th><th>"+val[2]+"</th><th>"+val[3]+"</th>");
	  			i++;
	  		}else{
	  			$("#sales_geochart tbody").append("<tr><td>"+val[0]+"</td><td>"+val[1]+"</td><td>"+val[2]+"</td><td>R$ "+val[3].toLocaleString('pt')+"</td></tr>");
	  		}
		}); 
	});

	$.getJSON( "js/top_products.json", function( data ) {
		
		$("#links-loja").append("<a href='#' class='no-border left-arrow'><img src='img/play-button-left.png' alt='seta esquerda' /></a>");
		$("#links-loja").append("<a href='todos' class='active'>Todos</a> ");
		i = 0;

	  	$.each( data.marketplaces, function( key, val ) {
	  		if(val.length>0){
	    		$("#links-loja").append("<a href='"+val+"'>"+val.replace("_"," ")+"</a> ");
	  		}

		  	$.each( data.products[val], function( key2, val2 ) {
		  		if(i==0){
		  			$("#top_products thead").append("<tr><td>Nome</td><td>Quantidade</td><td>Receita</td></tr>");
		  			i++;
			  	}
	  			$("#top_products tbody").append("<tr class='"+val+"'><td>"+val2.name+"</td><td>"+val2.qtd_sold+"</td><td>R$ "+val2.total_money.toLocaleString('pt')+"</td></tr>");
		  	});
	  	});
		$("#links-loja").append("<a href='#' class='no-border right-arrow'><img src='img/play-button-right.png' alt='seta direita' /></a>");
		$("#links-loja a").on("click",(function(event){
			if($(this).hasClass("right-arrow")){
				nextLink = $("#links-loja a.active").next().click();
				event.preventDefault(event);
			}else if($(this).hasClass("left-arrow")){
				nextLink = $("#links-loja a.active").prev().click();
				event.preventDefault(event);
			}
			else{			
				$("#links-loja a").removeClass("active");
				$(this).addClass("active");
				event.preventDefault(event);
				classe = $(this).attr("href");
				$("#top_products tbody tr").hide();
				if(classe=="todos"){
					$("#top_products .no-table").hide();
					$("#top_products tbody tr").show();	
				}else{
					if($("#top_products tbody tr."+classe).length>0){
						$("#top_products tbody tr."+classe).show();
					}else{
						$("#top_products .no-table").show();
					}
				}
			}
		}));
		google.charts.setOnLoadCallback(drawLogScales);

		function drawLogScales() {
	    	  var data = new google.visualization.DataTable();
		      data.addColumn('number', 'X');
		      data.addColumn('number', 'Magento');
		      data.addColumn('number', 'Extra');
		      data.addColumn('number', 'Walmart');
		      data.addColumn('number', 'Mercado Livre');
		      data.addColumn('number', 'Submarino');
		      data.addColumn('number', 'Lojas');
		      data.addColumn('number', 'Americanas');

		    //popula o gráfico aleatoriamente


			function randomIntFromInterval(min,max)
			{
			    return Math.floor(Math.random()*(max-min+1)+min);
			}

	      var grafico = [];
	      for (var i = 1; i < 10; i++) {
	      	grafico[i-1] = [i,randomIntFromInterval(30,60),randomIntFromInterval(30,60),randomIntFromInterval(30,60),randomIntFromInterval(30,60),randomIntFromInterval(30,60),randomIntFromInterval(30,60),randomIntFromInterval(30,60)]
	      }
	      data.addRows(grafico);

	      var options = {
	        hAxis: {
	          title: 'Valor',
	          logScale: false
	        },
	        vAxis: {
	          title: 'Dia',
	          logScale: false
	        },
	        colors: ['#32bee3', '#f596a2','#96f5ac', '#e9f596','#f5d596', '#d96859','#d959cb', '#f93939']
	      };

	      var chart = new google.visualization.LineChart(document.getElementById('chart_div_first'));
	      chart.draw(data, options);
	    }

		    //so recarrega o gráfico quando acaba de dar resize para evitar várias chamadas
			$(window).resize(function() {
			    if(this.resizeTO) clearTimeout(this.resizeTO);
			    this.resizeTO = setTimeout(function() {
			        $(this).trigger('resizeEnd');
			    }, 500);
			});

			//redesenha o gráfico
			$(window).on('resizeEnd', function() {
			    drawStacked();
			    drawLogScales()
			});


		google.charts.setOnLoadCallback(drawStacked);

		function drawStacked() {
			$.getJSON( "js/breakdown_chart.json", function( data ) {
				
			  var data = google.visualization.arrayToDataTable(data);
			  
		      var options = {
		        chartArea: {width: '50%'},
		        isStacked: true,
		        
		      };
		      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
		      chart.draw(data, options);
			});

		    }

	  		
	});
});